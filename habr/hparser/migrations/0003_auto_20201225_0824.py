# Generated by Django 3.1.4 on 2020-12-25 08:24

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hparser', '0002_result'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='result',
            options={'verbose_name': 'Результат', 'verbose_name_plural': 'Результаты'},
        ),
    ]
