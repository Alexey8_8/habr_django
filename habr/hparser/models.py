from django.db import models


class Task(models.Model):
    url = models.URLField(
        verbose_name='Ссылка на хаб'
    )

    def __str__(self):
        return f'#{self.url}'

    class Meta:
        verbose_name = 'Задание'
        verbose_name_plural = 'Задания'


class Result(models.Model):
    date = models.TextField(
        verbose_name='Дата'
    )

    author_name = models.TextField(
        verbose_name='Имя автора'
    )

    nick_name = models.TextField(
        verbose_name='Ник автора'
    )

    name_link = models.URLField(
        verbose_name='Ссылка на профиль автора'
    )

    title = models.TextField(
        verbose_name='Название статьи'
    )

    project_urls = models.URLField(
        verbose_name='Ссылка на статью'
    )

    text = models.TextField(
        verbose_name='Текст статьи'
    )

    def __str__(self):
        return f'#{self.date} {self.author_name} {self.nick_name} {self.name_link} {self.title} {self.project_urls} {self.text}'

    class Meta:
        verbose_name = 'Результат'
        verbose_name_plural = 'Результаты'
