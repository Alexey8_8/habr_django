from django.contrib import admin

from .models import Task
from .models import Result


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = ('id', 'url')


@admin.register(Result)
class ResultAdmin(admin.ModelAdmin):
    list_display = ('date', 'author_name', 'nick_name', 'name_link', 'title', 'project_urls', 'text')


