from django.apps import AppConfig


class HparserConfig(AppConfig):
    name = 'hparser'
